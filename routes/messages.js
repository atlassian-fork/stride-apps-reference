const express = require('express');
const router = express.Router();

const logger = require('../middleware/logger').logger;
const stride = require('../client');
let helpers = require('../helpers');

/**
 * @name Messages: send a direct message to a user
 * @description
 * Since sending a message is done using a conversation ID, the the first step is to obtain the conversation ID for the direct conversation.
 * You can then use the "send a message to a conversation" endpoint
 * @see {@link https://developer.atlassian.com/cloud/stride/rest/#rest-site-cloudId-conversation-user-userId-message-post | API Reference: Direct Messages }
 * @see {@link https://developer.atlassian.com/cloud/stride/learning/messages/ | Concept Guide }
 * @see {@link https://developer.atlassian.com/cloud/stride/learning/sending-direct-messages-to-users/| How-to Guide }
 */
router.post('/direct', async (req, res, next) => {
  const loggerInfoName = 'message_send_direct';

  try {
    const { cloudId, userId } = res.locals.context;

    logger.info(`${loggerInfoName}:direct incoming request for user ${userId}`);

    let conversation = await stride.api.conversations.getByUserId(cloudId, userId, {});

    let optsDoc = {
      body: 'I just sent you a direct message.',
      headers: { 'Content-Type': 'text/plain', accept: 'application/json' },
    };
    await stride.api.messages.sendMessage(cloudId, conversation.id, optsDoc);
    res.sendStatus(204);
  } catch (err) {
    logger.error(`${loggerInfoName} error: ${err}`);
    res.sendStatus(500);
    next(err);
  }
});

/**
 * @name Messages: send a text message
 * @description
 * Send a plain text message.  Headers.Content-Type must be set to text/plain.
 * @see {@link https://developer.atlassian.com/cloud/stride/rest/#rest-site-cloudId-conversation-user-userId-message-post | API Reference: Messages }
 * @see {@link https://developer.atlassian.com/cloud/stride/learning/messages/ | Concept Guide }
 * @see {@link https://developer.atlassian.com/cloud/stride/learning/sending-messages/#send-plain-text | How-to Guide }
 */
router.post('/textFormat', async (req, res, next) => {
  let loggerInfoName = 'message_text_conversation';

  try {
    const { cloudId, conversationId } = res.locals.context;
    logger.info(`${loggerInfoName} incoming request for ${conversationId}`);

    let opts = {
      body:
        'I just sent you a plain text message.  Remember, Content-Type must be set to text/plain.',
      headers: { 'Content-Type': 'text/plain', accept: 'application/json' },
    };
    await stride.api.messages.sendMessage(cloudId, conversationId, opts);

    res.sendStatus(204);
  } catch (err) {
    logger.error(`${loggerInfoName} error: ${err}`);
    res.sendStatus(500);
    next(err);
  }
});

/**
 * @name Messages: send a message with rich format
 * @description
 * Send a message in Rich Text Format.
 * @see {@link https://developer.atlassian.com/cloud/stride/rest/#rest-site-cloudId-conversation-user-userId-message-post | API Reference: Messages }
 * @see {@link   https://developer.atlassian.com/cloud/stride/learning/messages/ | Concept Guide }
 * @see {@link   https://developer.atlassian.com/cloud/stride/learning/formatting-messages/ | How-to Guide }
 * @see {@link   https://developer.atlassian.com/cloud/stride/blocks/message-format/ | Building Block }
 */
router.get('/richFormatMessage', async (req, res, next) => {
  const loggerInfoName = 'format_message_variety';

  try {
    const { cloudId, conversationId } = res.locals.context;
    logger.info(`${loggerInfoName} incoming request for ${conversationId}`);

    const message = helpers.format.messageWithRichFormat();
    await stride.api.messages.sendMessage(cloudId, conversationId, { body: message });

    res.sendStatus(204);
  } catch (err) {
    logger.error(`${loggerInfoName} error: ${err}`);
    next(err);
  }
});

/**
 * @name Messages: update a message
 * @description
 * Update a message.
 * @see {@link https://developer.atlassian.com/cloud/stride/rest/#api-site-cloudId-conversation-conversationId-message-messageId-put | API Reference: Update Conversation Message }
 * @see {@link https://developer.atlassian.com/cloud/stride/rest/#api-site-cloudId-conversation-user-userId-message-messageId-put  | API Reference: Update Direct Message }
 * @see {@link   https://developer.atlassian.com/cloud/stride/learning/messages/ | Concept Guide }
 */
router.post('/edit', async (req, res, next) => {
  let loggerInfoName = 'message_update_conversation';

  try {
    const { cloudId, conversationId } = res.locals.context;
    logger.info(`${loggerInfoName} incoming request for ${conversationId}`);

    // First, send a message

    const opts = {
      body: 'How do you spell tomfoulery?',
      headers: { 'Content-Type': 'text/plain' },
    };
    let messageSent = await stride.api.messages.sendMessage(cloudId, conversationId, opts);

    // Now edit this message
    let messageSentObj = JSON.parse(messageSent);
    const messageId = messageSentObj.id;
    const editOpts = {
      body: `It's spelt tomfoolery. I just edited this message`,
      headers: { 'Content-Type': 'text/plain' },
    };
    await stride.api.messages.updateMessage(cloudId, conversationId, messageId, editOpts);

    res.sendStatus(204);
  } catch (err) {
    logger.error(`${loggerInfoName} error: ${err}`);
    res.sendStatus(500);
    next(err);
  }
});

/**
 * @name Messages: delete a message
 * @description
 * Delete an existing message.
 * @see {@link https://developer.atlassian.com/cloud/stride/rest/#api-site-cloudId-conversation-conversationId-message-messageId-delete | API Reference: Delete Conversation Message }
 * @see {@link https://developer.atlassian.com/cloud/stride/rest/#api-site-cloudId-conversation-user-userId-message-messageId-delete | API Reference: Delete Direct Message }
 * @see {@link   https://developer.atlassian.com/cloud/stride/learning/messages/ | Concept Guide }
 */
router.post('/delete', async (req, res, next) => {
  let loggerInfoName = 'message_delete_in_conversation';

  try {
    const { cloudId, conversationId } = res.locals.context;
    logger.info(`${loggerInfoName} incoming request for ${conversationId}`);

    // Create and Send Text Message
    let opts = {
      body: 'This message is about to be deleted, watch out!',
      headers: { 'Content-Type': 'text/plain' },
    };

    let messageSent = await stride.api.messages.sendMessage(cloudId, conversationId, opts);
    let messageSentObj = JSON.parse(messageSent);
    let messageId = messageSentObj.id;

    await stride.api.messages.deleteMessage(cloudId, conversationId, messageId, {});

    res.sendStatus(204);
  } catch (err) {
    logger.error(`${loggerInfoName} error: ${err}`);
    res.sendStatus(500);
    next(err);
  }
});

/**
 * @name Messages: send a markdown message
 * @description
 * Send a markdown message.  Headers.Content-Type must be set to text/markdown.
 * @see {@link https://developer.atlassian.com/cloud/stride/rest/#rest-site-cloudId-conversation-user-userId-message-post | API Reference: Messages }
 * @see {@link https://developer.atlassian.com/cloud/stride/learning/messages/ | Concept Guide }
 * @see {@link https://developer.atlassian.com/cloud/stride/learning/sending-messages/| How-to Guide }
 * @see {@link https://developer.atlassian.com/cloud/stride/learning/sending-messages-as-markdown/ | Sending messages as Markdown }
 */
router.post('/textMarkdownFormat', async (req, res, next) => {
  let loggerInfoName = 'message_markdown_conversation';

  try {
    const { cloudId, conversationId } = res.locals.context;
    logger.info(`${loggerInfoName} incoming request for ${conversationId}`);

    // Create and Send Text Message
    let opts = {
      body: `I just sent you a **_markdown_** message.  Remember, Content-Type must be set to text/markdown.`,
      headers: { 'Content-Type': 'text/markdown', accept: 'application/json' },
    };

    await stride.api.messages.sendMessage(cloudId, conversationId, opts);

    res.sendStatus(204);
  } catch (err) {
    logger.error(`${loggerInfoName} error: ${err}`);
    res.sendStatus(500);
    next(err);
  }
});

module.exports = router;
