const express = require('express');
const router = express.Router();
const cors = require('cors');
const util = require('util');

const stride = require('../client');
const logger = require('../middleware/logger').logger;
let helpers = require('../helpers');
const { Document } = require('adf-builder');

router.options('/action/handleMessage', cors(), (req, res) => {
  res.sendStatus(200);
});
router.options('/action/handleCardAction', cors(), (req, res) => {
  res.sendStatus(200);
});
router.options('/action/updateCard', cors(), (req, res) => {
  res.sendStatus(200);
});
router.options('/action/handleInlineMessageAction', cors(), (req, res) => {
  res.sendStatus(200);
});
router.options('/action/handleInlineMessageSelect', cors(), (req, res) => {
  res.sendStatus(200);
});
router.options('/action/handleExpandAction', cors(), (req, res) => {
  res.sendStatus(200);
});

/**
 * @name Actions: message with a card
 * @see {@link https://developer.atlassian.com/cloud/stride/learning/actions/ | Concept Guide}
 * @see {@link https://developer.atlassian.com/cloud/stride/learning/adding-actions/ | How-to Guide}
 */
router.post('/actionCard', async (req, res, next) => {
  let loggerInfoName = 'action_card';
  logger.info(`${loggerInfoName} incoming request`);

  try {
    const { cloudId, conversationId } = res.locals.context;

    let message = helpers.actions.messageWithCard();
    await stride.api.messages.sendMessage(cloudId, conversationId, { body: message });
    res.sendStatus(204);
  } catch (err) {
    res.sendStatus(500);
    logger.info(`${loggerInfoName} error: ${err}`);
    next(err);
  }
});

router.post('/action/handleCardAction', cors(), async (req, res, next) => {
  const loggerInfoName = 'action_service';

  try {
    const { conversationId } = res.locals.context;
    logger.info(`${loggerInfoName} incoming call for ${conversationId}`);

    let response = {
      message: 'Done!',
      nextAction: {},
    };

    const parameters = req.body.parameters;
    if (parameters && parameters.then === 'openDialog')
      response.nextAction = {
        target: {
          key: 'actionTarget-sendToDialog',
        },
      };

    res.json(response);
  } catch (err) {
    logger.error(`${loggerInfoName} error handling action: ${err}`);
    res.sendStatus(500);
    next(err);
  }
});

router.post('/action/handleMessage', cors(), async (req, res, next) => {
  const loggerInfoName = 'action_handle_message';

  const { cloudId, conversationId } = res.locals.context;
  logger.info(`${loggerInfoName} incoming call for ${conversationId}`);

  try {
    var messageToAction = req.body.context.message.body;
    helpers.actions.ackMessage(messageToAction);
    let opts = {
      body: messageToAction,
      headers: { 'Content-Type': 'Application/Json' },
    };
    stride.api.messages.sendMessage(cloudId, conversationId, opts);

    res.sendStatus(204);
  } catch (err) {
    res.sendStatus(500);
    logger.error(`${loggerInfoName} found an error: ${err} `);
    next(err);
  }
});

/**
 * @name Actions: Message with a card which updates when an action is clicked
 * @see {@link https://developer.atlassian.com/cloud/stride/learning/actions/ | Concept Guide}
 * @see {@link https://developer.atlassian.com/cloud/stride/learning/adding-actions/ | How-to Guide}
 */

router.post('/updateCard', cors(), async (req, res, next) => {
  const loggerInfoName = 'send_update_card';

  try {
    logger.info(`${loggerInfoName} incoming with request body ${util.format(req.body)}`);

    const { cloudId, conversationId } = res.locals.context;

    let message = helpers.actions.messageWithCardThatUpdates();
    await stride.api.messages.sendMessage(cloudId, conversationId, { body: message });

    res.json({});
  } catch (err) {
    res.sendStatus(500);
    logger.error(`${loggerInfoName} found an error: ${err} `);
    next(err);
  }
});

router.post('/action/updateCard', async (req, res, next) => {
  const loggerInfoName = 'update_action_card_message';

  try {
    const { cloudId, conversationId } = res.locals.context;

    const messageId = req.body.context.message.mid;
    const parameters = req.body.parameters;

    let doc = new Document();

    let card = doc
      .applicationCard('Incident #4253')
      .link('https://www.helpers.com')
      .description('Something is broken');

    if (parameters.incidentAction === 'ack') {
      card
        .detail()
        .title('Status')
        .text('In progress');
      card
        .detail()
        .title('Assigned to')
        .text('Joe Blog');
      card
        .action()
        .title('Resolve')
        .target({ key: 'actionTarget-updateCard' })
        .parameters({ incidentAction: 'resolve' });
    }

    if (parameters.incidentAction === 'resolve') {
      card
        .detail()
        .title('Status')
        .text('Resolved');
      card
        .action()
        .title('Reopen')
        .target({ key: 'actionTarget-updateCard' })
        .parameters({ incidentAction: 'reopen' });
    }

    if (parameters.incidentAction === 'reopen') {
      card
        .detail()
        .title('Status')
        .text('Reopened');
      card
        .action()
        .title('Ack')
        .target({ key: 'actionTarget-updateCard' })
        .parameters({ incidentAction: 'ack' });
      card
        .action()
        .title('Resolve')
        .target({ key: 'actionTarget-updateCard' })
        .parameters({ incidentAction: 'resolve' });
    }

    card.context('DevOps / Incidents').icon({
      url: 'https://image.ibb.co/fPPAB5/Stride_White_On_Blue.png',
      label: 'Stride',
    });

    let document = doc.toJSON();
    let opts = {
      body: document,
      headers: { 'Content-Type': 'Application/Json' },
    };

    await stride.api.messages.updateMessage(cloudId, conversationId, messageId, opts);

    res.json({});
  } catch (err) {
    logger.error(`${loggerInfoName} found an error: ${err} `);
    res.sendStatus(500);
    next(err);
  }
});

/**
 * @name Actions: Message with a link that triggers an action
 * @see {@link https://developer.atlassian.com/cloud/stride/learning/actions/ | Concept Guide}
 * @see {@link https://developer.atlassian.com/cloud/stride/learning/adding-actions/ | How-to Guide}
 */

router.post('/actionMark', cors(), async (req, res, next) => {
  const loggerInfoName = 'send_message_with_actionmark';

  try {
    logger.info(`${loggerInfoName} incoming with request body ${util.format(req.body)}`);

    const { cloudId, conversationId } = res.locals.context;

    let message = helpers.actions.messageWithActionMark();
    let opts = {
      body: message,
      headers: { 'Content-Type': 'Application/Json' },
    };
    await stride.api.messages.sendMessage(cloudId, conversationId, opts);

    res.sendStatus(204);
  } catch (err) {
    res.sendStatus(500);
    logger.error(`${loggerInfoName} found an error: ${err} `);
    next(err);
  }
});

/**
 * Message with inline actions
 */
router.post('/inlineMessageAction', async (req, res, next) => {
  let loggerInfoName = 'inline_message_actions';
  logger.info(`${loggerInfoName} incoming request`);

  try {
    const { cloudId, conversationId } = res.locals.context;

    let messageWithInlineActions = helpers.actions.messageWithInlineActionGroup();
    let messageOpts = {
      body: messageWithInlineActions,
    };
    await stride.api.messages.sendMessage(cloudId, conversationId, messageOpts);

    res.sendStatus(204);
  } catch (err) {
    res.sendStatus(500);
    logger.info(`${loggerInfoName} error: ${err}`);
    next(err);
  }
});

router.post('/action/handleInlineMessageAction', cors(), async (req, res, next) => {
  const loggerInfoName = 'inline_message_actions_clicked';

  try {
    const { cloudId, conversationId, userId } = res.locals.context;
    logger.info(`${loggerInfoName} incoming call for ${conversationId}`);

    const reason = req.body.source;

    let replacementMessage = helpers.actions.messageWithInlineActionGroupResponse(userId, reason);
    var messageToEditId = req.body.context.message.mid;
    let messageOpts = {
      body: replacementMessage,
    };
    await stride.api.messages.updateMessage(cloudId, conversationId, messageToEditId, messageOpts);

    res.json({});
  } catch (err) {
    logger.error(`${loggerInfoName}: error handling action: ${err}`);
    res.sendStatus(500);
    next(err);
  }
});

/**
 * Inline select
 */

router.post('/inlineMessageSelect', async (req, res, next) => {
  let loggerInfoName = 'inline_message_select';
  logger.info(`${loggerInfoName} incoming request`);

  try {
    const { cloudId, conversationId } = res.locals.context;

    let messageWithInlineSelect = helpers.actions.messageWithInlineSelect();
    let messageOpts = {
      body: messageWithInlineSelect,
    };
    await stride.api.messages.sendMessage(cloudId, conversationId, messageOpts);

    res.sendStatus(204);
  } catch (err) {
    res.sendStatus(500);
    logger.info(`${loggerInfoName} error: ${err}`);
    next(err);
  }
});

router.post('/action/handleInlineMessageSelect', cors(), async (req, res, next) => {
  const loggerInfoName = 'inline_select_clicked';

  try {
    const { cloudId, conversationId } = res.locals.context;
    logger.info(`${loggerInfoName} incoming call for ${conversationId}`);

    const parameters = req.body.parameters;

    var response = {
      nextAction: '',
    };

    if (parameters) {
      if (parameters.selectedValue) {
        if (parameters.selectedValue === 'openSidebar') {
          response.nextAction = {
            target: {
              key: 'actionTarget-openSidebar-showcase',
            },
          };
        }
        if (parameters.selectedValue === 'openDialog') {
          response.nextAction = {
            target: {
              key: 'actionTarget-sendToDialog',
            },
          };
        }
        if (parameters.selectedValue === 'openRoom') {
          var opts = {
            body: {
              name: 'Stride Reference App test room' + new Date().getTime(),
              privacy: 'public',
              topic: 'This room was created via the API',
            },
          };

          let createConversation = await stride.api.conversations.create(cloudId, opts);

          let createdRoom = await createConversation;

          response.nextAction = {
            target: {
              receiver: 'stride',
              key: 'openConversation',
            },
            parameters: {
              conversationId: createdRoom.id,
            },
          };
        }
        if (parameters.selectedValue === 'openHighlights') {
          response.nextAction = {
            target: {
              receiver: 'stride',
              key: 'openHighlights',
            },
          };
        }
        if (parameters.selectedValue === 'openFiles') {
          response.nextAction = {
            target: {
              receiver: 'stride',
              key: 'openFilesAndLinks',
            },
          };
        }
      }
    }

    res.json(response);
  } catch (err) {
    logger.error(`${loggerInfoName}:action error: ${err}`);
    res.sendStatus(500);
    next(err);
  }
});

/**
 * Expand
 */
router.post('/expand', async (req, res, next) => {
  let loggerInfoName = 'expand';
  logger.info(`${loggerInfoName} incoming request`);

  console.log(req.query);

  try {
    const { cloudId, conversationId, userId } = res.locals.context;
    let conditions;

    if (req.query.isOpened) {
      conditions = [
        {
          condition: 'expandedForUsers',
          params: {
            users: [userId],
          },
          invert: false,
        },
      ];
    }

    let messageWithExpandNode = helpers.actions.messageWithExpandNode(userId, conditions);
    let messageOpts = {
      body: messageWithExpandNode,
    };
    await stride.api.messages.sendMessage(cloudId, conversationId, messageOpts);

    res.sendStatus(204);
  } catch (err) {
    res.sendStatus(500);
    logger.info(`${loggerInfoName} error: ${err}`);
    next(err);
  }
});

router.post('/action/handleExpandAction', cors(), async (req, res, next) => {
  const loggerInfoName = 'expand_inline_actions_clicked';

  try {
    const { cloudId, conversationId, userId } = res.locals.context;
    logger.info(`${loggerInfoName} incoming call for ${conversationId}`);

    const reason = req.body.source;

    let replacementMessage = helpers.actions.messageWithExpandNode(userId, null, reason);
    var messageToEditId = req.body.context.message.mid;
    let messageOpts = {
      body: replacementMessage,
    };
    await stride.api.messages.updateMessage(cloudId, conversationId, messageToEditId, messageOpts);

    res.json({});
  } catch (err) {
    logger.error(`${loggerInfoName}: error handling action: ${err}`);
    res.sendStatus(500);
    next(err);
  }
});

module.exports = router;
