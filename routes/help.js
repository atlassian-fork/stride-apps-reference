const express = require('express');
const router = express.Router();
const logger = require('../middleware/logger').logger;
const stride = require('../client');
let helpers = require('../helpers');

router.get('/menu', async (req, res, next) => {
  const loggerInfoName = 'message_help_menu';

  try {
    const { cloudId, conversationId } = res.locals.context;

    logger.info(`${loggerInfoName} incoming call for ${conversationId}`);

    //Then send a message with the help menu
    const helpMenuBody = helpers.format.helpMenu();
    let opts = { body: helpMenuBody };
    await stride.api.messages.sendMessage(cloudId, conversationId, opts);

    res.sendStatus(204);
  } catch (err) {
    logger.error(`${loggerInfoName} error found: ${err}`);
    res.sendStatus(500);
    next(err);
  }
});

module.exports = router;
